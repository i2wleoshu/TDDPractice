//
//  TDDPracticeTests.swift
//  TDDPracticeTests
//
//  Created by Leo_hsu on 2018/6/9.
//  Copyright © 2018年 Gemtek Technology Co., Ltd. All rights reserved.
//

import XCTest
@testable import TDDPractice

class AccountantTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_one_budget_in_whole_month() {
        let amountInpt: Double = 31
        let budget = Budget(yearMonth: "2018/1", amount: amountInpt)
        let period = Period(startDay: "2018/1/1", endDay: "2018/1/31")
        let accountant = Accountant()
        let calResult = accountant.calculate([budget], period)
        XCTAssertEqual(amountInpt, calResult)
    }
    
    func test_one_budget_in_whole_month2() {
        let amountInput: Double = 30
        let budget = Budget(yearMonth: "2018/1", amount: amountInput)
        let period = Period(startDay: "2018/1/1", endDay: "2018/1/31")
        let accountant = Accountant()
        let calResult = accountant.calculate([budget], period)
        XCTAssertEqual(amountInput, calResult)
    }

    func test_one_budegt_in_half_month() {
        let budget = Budget(yearMonth: "2018/1", amount: 31.0)
        let period = Period(startDay: "2018/1/1", endDay: "2018/1/15")
        let accountant = Accountant()
        let calResult = accountant.calculate([budget], period)
        XCTAssertEqual(31*(15/31), calResult)
    }

    func test_one_budegt_in_half_month2() {
        let budget = Budget(yearMonth: "2018/2", amount: 31.0)
        let period = Period(startDay: "2018/2/1", endDay: "2018/2/14")
        let accountant = Accountant()
        let calResult = accountant.calculate([budget], period)
        XCTAssertEqual(31*(14/28), calResult)
    }

    func test_one_budegt_not_in_month() {
        let budget = Budget(yearMonth: "2018/1", amount: 31.0)
        let period = Period(startDay: "2018/2/1", endDay: "2018/2/28")
        let accountant = Accountant()
        let calResult = accountant.calculate([budget], period)
        XCTAssertEqual(0, calResult)
    }
    
    func test_two_budget_in_whole_month() {
        let budget1 = Budget(yearMonth: "2018/1", amount: 31.0)
        let budget2 = Budget(yearMonth: "2018/2", amount: 28.0)
        let period = Period(startDay: "2018/2/1", endDay: "2018/2/28")
        let accountant = Accountant()
        let calResult = accountant.calculate([budget1, budget2], period)
        XCTAssertEqual(28, calResult)
    }
    
    func test_two_budget_not_in_month() {
        let budget1 = Budget(yearMonth: "2018/1", amount: 31.0)
        let budget2 = Budget(yearMonth: "2018/2", amount: 28.0)
        let period = Period(startDay: "2018/3/1", endDay: "2018/3/31")
        let accountant = Accountant()
        let calResult = accountant.calculate([budget1, budget2], period)
        XCTAssertEqual(0, calResult)
    }
    
    func test_two_budget_in_half_month() {
        let budget1 = Budget(yearMonth: "2018/1", amount: 31.0)
        let budget2 = Budget(yearMonth: "2018/2", amount: 28.0)
        let period = Period(startDay: "2018/2/1", endDay: "2018/2/14")
        let accountant = Accountant()
        let calResult = accountant.calculate([budget1, budget2], period)
        XCTAssertEqual(14, calResult)
    }
    
    func test_two_budget_in_two_half_month() {
        let budget1 = Budget(yearMonth: "2018/1", amount: 31.0)
        let budget2 = Budget(yearMonth: "2018/2", amount: 28.0)
        let period = Period(startDay: "2018/1/17", endDay: "2018/2/14")
        let accountant = Accountant()
        let calResult = accountant.calculate([budget1, budget2], period)
        let expect: Double = 31*(15/31) + 28*(14/28)
        XCTAssertEqual(expect, calResult)
    }

}

