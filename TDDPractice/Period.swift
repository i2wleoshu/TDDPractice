//
// Created by Leo_hsu on 2018/6/15.
// Copyright (c) 2018 Gemtek Technology Co., Ltd. All rights reserved.
//

import Foundation

let preDay: Double = 24*60*60

class Period {
    let starDay: Date
    let endDay: Date

    init(startDay: String, endDay: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 8)
        let beginDate = dateFormatter.date(from: startDay)
        assert(beginDate != nil, "starDay fromat is wrong")
        self.starDay = beginDate!
        let endDate = dateFormatter.date(from: endDay)
        assert(endDate != nil, "endDay fromat is wrong")
        self.endDay = endDate!
    }

    func intersect(budgetsPeriod: Period) -> Double {
        if budgetsPeriod.starDay > self.endDay ||  budgetsPeriod.endDay < self.starDay {
            return 0
        }
        var intersectFrom: Date = Date.init()
        var intersectEnd: Date = Date.init()
        if budgetsPeriod.starDay < self.starDay {
            intersectFrom = self.starDay
        } else {
            intersectFrom = budgetsPeriod.starDay
        }
        if budgetsPeriod.endDay < self.endDay {
            intersectEnd = budgetsPeriod.endDay
        } else {
            intersectEnd = self.endDay
        }
        
        return (intersectEnd.timeIntervalSince(intersectFrom) / preDay) + 1
    }

}
