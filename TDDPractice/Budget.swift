//
// Created by Leo_hsu on 2018/6/9.
// Copyright (c) 2018 Gemtek Technology Co., Ltd. All rights reserved.
//

import Foundation

class Budget {

    let yearMonth: Date
    let amountInput: Double

    init(yearMonth: String, amount: Double) {
        self.amountInput = amount
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM"
        dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 8)
        let date = dateFormatter.date(from: yearMonth)
        assert(date != nil, "yearMonth fromat is wrong")
        self.yearMonth = date!
    }

    func getTotalDays() -> Double {
        let calendar = Calendar.current
        let range = calendar.range(of: .day, in: .month, for: self.yearMonth)!
        return Double(range.count)
    }

    func getBudgetPeriodDay() -> Period {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.init(secondsFromGMT: 8)!
        let components = calendar.dateComponents([.year, .month], from: self.yearMonth)
        let startOfMonth = calendar.date(from: components)!
        var comps2 = DateComponents()
        comps2.month = 1
        comps2.day = -1
        let endOfMonth = calendar.date(byAdding: comps2, to: startOfMonth)!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        return Period.init(startDay: dateFormatter.string(from: startOfMonth), endDay: dateFormatter.string(from: endOfMonth))
    }

}
