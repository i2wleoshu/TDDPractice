//
// Created by Leo_hsu on 2018/6/9.
// Copyright (c) 2018 Gemtek Technology Co., Ltd. All rights reserved.
//

import Foundation

class Accountant {

    func calculate(_ budgets: [Budget], _ period: Period) -> Double {
        var amountOutput: Double = 0
        for budget in budgets {
            let budgetPeriod = budget.getBudgetPeriodDay()
            let durationDays = period.intersect(budgetsPeriod: budgetPeriod)
            let totalDays = budget.getTotalDays()
            let ratio = durationDays / totalDays
            amountOutput += budget.amountInput * ratio
        }
        return amountOutput
    }
    
    
}
